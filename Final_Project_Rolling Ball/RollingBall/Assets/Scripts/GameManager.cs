﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Button startButton;
    [SerializeField] private RectTransform dialog;

    private void Awake()
    {
        Debug.Assert(startButton != null, "startButton cannot be null");
        Debug.Assert(dialog != null, "dialog cannot be null");
        
        
        startButton.onClick.AddListener(OnStartButtonClicked);
    }
    
    
    private void OnStartButtonClicked()
    {
        dialog.gameObject.SetActive(false);
        StartGame();
    }

    private void StartGame()
    {
        
    }
    
}


