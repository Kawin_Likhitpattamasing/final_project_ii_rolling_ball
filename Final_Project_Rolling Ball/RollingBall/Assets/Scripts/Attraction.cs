﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attraction : MonoBehaviour
{
    public Rigidbody mRigidbody;

    public float Gravity = 6.67f;
    

    void GravityAttraction( Attraction  ObjToAttractor)
    {
        Rigidbody mRBToAttractor = ObjToAttractor.mRigidbody;
        
        Vector3 dir = mRigidbody.position - mRBToAttractor.position;
        
        float distance = dir.magnitude;
        
        float forceMagnitude = Gravity * ( (mRigidbody.mass * mRBToAttractor.mass)/Mathf.Pow(distance,2) );
        
        Vector3 force = dir.normalized * forceMagnitude;
        
        mRBToAttractor.AddForce(force);
    }
    void FixedUpdate()
    {
        Attraction[] Attractors = FindObjectsOfType<Attraction>();

        foreach (Attraction AllPlanet in Attractors)
        {
            if(AllPlanet != this)
                GravityAttraction(AllPlanet);
                
        }
    }
    
}
