﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPiano : MonoBehaviour
{
    [SerializeField] string strTag;
    [SerializeField] private AudioClip PianoSound;
    [SerializeField] private float PianoSoundSoundVolume = 0.3f;
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == strTag)
        {
            AudioSource.PlayClipAtPoint(PianoSound,Camera.main.transform.position,PianoSoundSoundVolume);
        }
    }
}
