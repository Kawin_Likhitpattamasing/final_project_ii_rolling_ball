﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inertia : MonoBehaviour
{
    private Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale += new Vector3(0, Input.GetAxis("Horizontal"), 0);
        print("inertia: " + rb.inertiaTensor);
        rb.angularVelocity = rb.inertiaTensor;

    }
}
